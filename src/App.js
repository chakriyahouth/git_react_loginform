import { BrowserRouter, Routes, Route, Navigate }  from "react-router-dom";

import Layout from "./modules/layout/layout";
import Login from "./modules/auth/Login";
import HomePage from "./modules/home";
import BlogPage from "./modules/blog/BlogPage";
import ContactUs from "./modules/contact/Contact";
import ErrorPage from "./modules/error/ErrorPage";
import ProductDetail from "./modules/products/components/ProductDetail";
import { useAuth } from "./modules/auth/Auth";
import "bootstrap/dist/css/bootstrap.min.css";


const App = () => {

// const isAuth = localStorage.getItem("token");
// console.log(isAuth);
const {auth} = useAuth();

// console.log(auth, "auth");
 
  return (
    <BrowserRouter>
      <Routes>
      {auth ? <Route path="/" element={<Layout />}>
          <Route index element={<HomePage />} />
          <Route path="blog" element={<BlogPage />} />
          <Route path="contactus" element={<ContactUs />} />
          <Route path="product/:id" element={<ProductDetail />} />
          <Route path="*" element={<Navigate to="/" />}></Route>
        </Route>
          :
          <>
          <Route index path="auth/Login" element={<Login />}></Route>
          <Route path="*" element={<Navigate to="/auth/login"/>} />
          </>
          
        }
         <Route path="error" element={<ErrorPage />} />

      </Routes>
    </BrowserRouter>
  );
};
export default App;
