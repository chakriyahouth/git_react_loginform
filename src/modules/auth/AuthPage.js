import {Outlet} from "react-router-dom";

function AuthPage(params){
    return <div>
        <Outlet/>  {/* why we use outlet, right here?*/}
    </div>
}

export default AuthPage;
