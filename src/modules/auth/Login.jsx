import {useState} from "react";
import { useAuth } from "./Auth";
import { login } from "./core/request";

// import axios from "axios";
// import HomePage from "../home";
// import BlogPage from "../blog/BlogPage";
// import Layout from "../layout/layout";

const Login = () => {
    const [username, setUsername] = useState("kminchelle");
    const [pass, setPass] = useState("0lelplR");
    const [error, setError] = useState("");
    const {saveAuth} = useAuth();

    const onLogin = (event) =>{
            event.preventDefault(); 
            login(username, pass).then((response) =>{
                setError("");
                saveAuth(response.data.token)
            }).catch((error) => {
                setError(error.response.data.token.message);
            })
        }





    // const onLogin=(event)=>{
    //     event.preventDefault();
    //     axios.post("https://dummyjson.com/auth/login",{
    //         username: username,
    //         password: pass,
    //     }).then((response)=>{
    //         // setJol(true)
    //         console.log(response)
    //         setError("");
    //         localStorage.setItem("token", response.data.token);
    //         window.location.reload();
    //     }).catch((error)=>{
    //         setError(error.response.data);
    //     })
    // }
    return <div className="container">
        {
            // jol ? (<Layout/>)
            // :
            <div className="row">`
            <div className="col-md-6 offset-md-3">
                <h2 className="text-center text-dark mt-5">Login Form</h2>
                <div className="text-center mb-5 text-dark">Made with bootstrap</div>
                <div className="card my-5">

                    <form className="card-body cardbody-color p-lg-5" style={{backgroundColor:"#F5EEF8"}}>

                        <div className="text-center">
                            <img src="https://www.dior.com/couture/var/dior/storage/images/horizon/logo-dior/logo/25334706-1-fre-FR/logo_1440_1200.jpg"
                                 className="img-fluid profile-image-pic img-thumbnail  my-3"
                                 width="200px" alt="profile"/>
                        </div>

                        <div className="mb-3">
                            <input type="text" className="form-control" id="Username" aria-describedby="emailHelp"
                                   placeholder="User Name" onChange={(e)=>setUsername(e.target.value)}/>
                        </div>
                        <div className="mb-3" >
                            <input type="password" className="form-control" id="password" placeholder="password" onChange={(e)=>setPass(e.target.value)}  />
                        </div>
                        <div className="text-center">
                            <button onClick={onLogin} className="btn btn-color px-5 mb-5 w-100">Login</button>
                        </div>
                        <div className="text-center text-danger">{error}</div>
                        <div id="emailHelp" className="form-text text-center mb-5 text-dark">Not
                            Registered? <a href="/" className="text-dark fw-bold"> Create an
                                Account</a>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        }
        
    </div>;
}

export default Login;
