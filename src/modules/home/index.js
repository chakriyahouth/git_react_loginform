import { useEffect } from "react";
import axios from "axios";
import ListProducts from "../products/components/ListProduct";

function HomePage({props}){

    useEffect(() =>{
        const getUser = () =>{
            axios.get("https://dummyjson.com/auth/me",{
                headers: {
                       "Authorization": `Bearer ${localStorage.getItem("token")}`
                }
            }).then((response) => {
                console.log(response)
            })
        }
        getUser();
    }, [])
    return <div>
        <ListProducts />
    </div>
}

export default HomePage;