// import { Link } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useAuth } from '../../auth/Auth';
import { getListProduct, searchProduct} from "../../products/core/request";
import { useState, useEffect } from "react";

function Header() {
  const {logout, setProducts} = useAuth();
    const [searchText, setSearchText] = useState("");
    

    useEffect(() => {
      getListProduct(100).then((response) =>{
          setProducts(response.data.products);
      });
    }, [setProducts]);
    const onSearch = () =>{
      searchProduct({q: searchText}).then((response) => {
        
          setProducts(response.data.products);
      });
      
     }

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container fluid>
        <Navbar.Brand href="#">KKPOB</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: '100px' }}
            navbarScroll
          >
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/blog">Blog</Nav.Link>
            
            <Nav.Link href="/contactus">Contact</Nav.Link>
            <Button variant="outline-success" onClick={logout}>Logout</Button>
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
              onChange={(e) => setSearchText(e.target.value)}
            />
            <Button variant="outline-success" onClick={onSearch}>Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;

